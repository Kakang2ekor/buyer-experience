const calculate = require('../lib/roiCalculate');

const { checkIfUltimate } = calculate;
const { addUserSpend } = calculate;
const { totalUserSpend } = calculate;
const { totalGitlabSpend } = calculate;

describe('roi calculator', () => {
  it('shows the user the Ultimate product if they enter values for portfolio management OR application security', () => {
    const agilePortfolioManagementSpend = 5600;
    const applicationSecuritySpend = 0;
    expect(
      checkIfUltimate(agilePortfolioManagementSpend, applicationSecuritySpend),
    ).toBe(true);
  });

  it('shows the user the Premium product if they do not enter portfolio management or application security amounts', () => {
    const agilePortfolioManagementSpend = 0;
    const applicationSecuritySpend = 0;
    expect(
      checkIfUltimate(agilePortfolioManagementSpend, applicationSecuritySpend),
    ).toBe(false);
  });

  it('adds user spending inputs together and returns a number', () => {
    const sourceCodeManagementSpend = 6500;
    const continuousIntegrationSpend = 65000;
    const continuousDeliverySpend = 0;
    const registriesSpend = '';
    const agileProjectManagementSpend = 3800;
    const agilePortfolioManagementSpend = 5600;
    const applicationSecuritySpend = 3200;

    expect(
      addUserSpend(
        sourceCodeManagementSpend,
        continuousIntegrationSpend,
        continuousDeliverySpend,
        registriesSpend,
        agileProjectManagementSpend,
        agilePortfolioManagementSpend,
        applicationSecuritySpend,
      ),
    ).toBe(84100);
  });

  it('calculates total user annual spend', () => {
    const numberOfMaintainers = 1;
    const sourceCodeManagementSpend = 6500;
    const continuousIntegrationSpend = 65000;
    const continuousDeliverySpend = 0;
    const registriesSpend = '';
    const agileProjectManagementSpend = 3800;
    const agilePortfolioManagementSpend = 5600;
    const applicationSecuritySpend = 3200;

    const sum = addUserSpend(
      sourceCodeManagementSpend,
      continuousIntegrationSpend,
      continuousDeliverySpend,
      registriesSpend,
      agileProjectManagementSpend,
      agilePortfolioManagementSpend,
      applicationSecuritySpend,
    );

    expect(totalUserSpend(sum, numberOfMaintainers)).toBe(181100);
  });

  it('calculates total annual spend with gitlab ultimate', () => {
    const agilePortfolioManagementSpend = 5600;
    const applicationSecuritySpend = 0;
    const numberOfUsers = 100;
    const gitlabMaintainers = 1;
    const isUltimate = checkIfUltimate(
      agilePortfolioManagementSpend,
      applicationSecuritySpend,
    );

    expect(totalGitlabSpend(isUltimate, numberOfUsers, gitlabMaintainers)).toBe(
      215800,
    );
  });

  it('calculates total annual spend with gitlab premium', () => {
    const agilePortfolioManagementSpend = 0;
    const applicationSecuritySpend = 0;
    const numberOfUsers = 100;
    const gitlabMaintainers = 1;
    const isUltimate = checkIfUltimate(
      agilePortfolioManagementSpend,
      applicationSecuritySpend,
    );

    expect(totalGitlabSpend(isUltimate, numberOfUsers, gitlabMaintainers)).toBe(
      119800,
    );
  });
});
